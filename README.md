# A Website for the Aktion Kindergarten Rotenburg e. V.

## Prerequisites:
* install wsl https://docs.microsoft.com/en-us/windows/wsl/install
* install Ubuntu 20.04 in wsl
* install git https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04
* install ruby and jekyll https://docs.github.com/en/pages/setting-up-a-github-pages-site-with-jekyll/creating-a-github-pages-site-with-jekyll
## Clone the website
* to clone the website execute the following command: `git clone git@gitlab.com:aktionkiga/website.git`
## Edit the website:
* start a local instance of the website `bundle exec jekyll serve`